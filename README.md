# DojoMadnes Code Challenge

An Angular 4 project to display champion SPI.

### personal note

It was a fun project and a good opportunity to learn new technologies
- angular 4
- TypeScript
- sass
I especially enjoyed building the spi ring

## Installing the project

### Prerequisites
- angular-cli 1.3
- npm 5.4
- node 6.11

### To run the project
- clone the repo
- ``` npm install ```
- ``` npm run start ```

## App architecture

- the index defines an angular app with a main component
	- app.component encapsulates the components of the app
		- navigation.component serves as a page title and navigator that always stays on screen regardless of the route changes
		- app routing module that defines which page should be displayed in the body
			- championList.component shows a list of all champions
			- championDetail.component shows the spi of the selected champion
	- champion.service that makes api requests and returns a promise
		- getList() gets a list of all champions with their details
		- getChampionById() gets details of a champion
		- getSPI gets spi of a champion
	- observer.service that notifies the navigation component to change its text
- constants: static values grouped into dedicated files for ease of access
	- styles.sass defines font faces to be used
	- vars.sass defines css values to make some modifications easier
	- strings.constants static text and urls that will be used in the app
- assets folder contains all the images to be used in the app
	- fonts
- api folder contains mock data for testing

### In-depth notes
- app
	- it is created using ``` ng new ```
	- only has a development environment
- navigation
	- kept it as a standalone component that is not directly affected by routing, for better user experience
	- changes text and layout by listening to route change and to the spi component emitting champion name to display
- list and spi pages
	- request their data through a service that makes api calls and returns a promise
	- include visual states to indicate waiting for data load and failure to do so
- detail (spi) page
	- gets champion id from route and retrieves the data from the api
	- an additional api request for getting champion basic info instead of sending the data from the list page insures that the data will be available even when the user refreshes the browser. So he will not be redirected to list page
	- applies a short delay to spi value update to ensure an animated filling of the spi ring
	- (per request) ringColor variable can be assigned any value from the component class to override the default color of the spi ring (this feature is currently not being used)

### Design notes
- since the app design was for a mobile screen, i catered for screen resizing, while making sure to keep the already defined sizes on the design
- i used rem unit instead of px in many css components to insure that different screen size and pixel density won't break the design. However, i didn't make them fully responsive to respect the predefined design values
- to insure design stability i limited the min-width and min-height of the pages to keep the structure intact when the screen is too small
- i used unicode characters for any icon that was not found in the assets. for example: &#767; for the back button in the navigation

### Unit testing
I researched and learned as much as i could about unit tests in the past couple of days, but the time restriction didnt allow me to build anything significant. I only had little time to experiment a bit with ``` app.component.spec.ts ```. But i will learn more about it next week.

# Thank You
