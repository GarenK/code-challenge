import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ObserverService {
  private _navTextSource = new BehaviorSubject<string>(null);

  navigationText$ = this._navTextSource.asObservable();

  changeNavigationText(text) {
    this._navTextSource.next(text);
  }
}
