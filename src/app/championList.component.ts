import { Component, OnInit } from '@angular/core';

import { ChampionService } from './champion.service';
import { Strings } from './strings.constants';

@Component({
  selector: 'app-champion-list',
  templateUrl: './championList.component.html',
  styleUrls: [ './championList.component.sass' ]
})
export class ChampionListComponent implements OnInit {
  linkTarget: string;
  error;
  championsList;

  constructor(
    private championService: ChampionService
  ) {
    this.linkTarget = `/${Strings.SPI_PAGE_NAME}/`;
  }

  ngOnInit(): void {
    this.championService.getList()
      .then(data => this.championsList = data.champion_analytics)
      .catch(error => this.error = error);
  }
}
