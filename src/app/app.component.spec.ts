import { TestBed, async, fakeAsync, ComponentFixture, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation.component';
import { ChampionListComponent } from './championList.component';
import { ChampionService } from './champion.service';
import { ObserverService } from './observer.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AppComponent,
        NavigationComponent
      ],
      providers: [
        ObserverService
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});

describe('NavigationComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        NavigationComponent
      ],
      providers: [
        ObserverService
      ],
    }).compileComponents();
  }));

  it(`should have as title 'DojoMadness Code Challenge' or contain 'SPI'`, async(() => {
    const fixture = TestBed.createComponent(NavigationComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.listPageTitle).toEqual('DojoMadness Code Challenge');
    expect(app.spiPageTitle).toContain('SPI');
  }));

  it('no champion name until loaded asyc', () => {
    const fixture = TestBed.createComponent(NavigationComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.titleComponent).toEqual(undefined);
  });

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(NavigationComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('DojoMadness Code Challenge');
  }));
});

describe('ChampionListComponent', () => {

  let comp: ChampionListComponent;
  let fixture: ComponentFixture<ChampionListComponent>;

  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let championService: ChampionService; // the actually injected service

  const testChampion = '{}';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpModule],
      declarations: [ ChampionListComponent ],
      providers:    [ ChampionService ],
    });

    fixture = TestBed.createComponent(ChampionListComponent);
    comp    = fixture.componentInstance;

    // ChampionService actually injected into the component
    championService = fixture.debugElement.injector.get(ChampionService);

    // Setup spy on the `getList` method
    spy = spyOn(championService, 'getList')
          .and.returnValue(Promise.resolve(testChampion));

    de = fixture.debugElement.query(By.css('.championName'));
  });

  it('champion should not exist before OnInit', () => {
    expect(de).toBe(null, 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getList not yet called');
  });

  it('should still not exist after component initialized', () => {
    fixture.detectChanges();
    expect(comp.championsList).toEqual(undefined);
    expect(de).toBe(null, 'no champion yet');
    expect(spy.calls.any()).toBe(true, 'getList called');
  });

  it('should show champion after getList promise (async)', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(() => { // wait for async getList
      fixture.detectChanges();        // update view with champion

      expect(spy.calls.any()).toBe(true, 'getList called');
      expect(comp.championsList).toEqual(jasmine.any(Object));
      el = de.nativeElement;
      expect(el.textContent).toBe('Aatrox');
    });
  }));

  it('should show champion after getList promise (fakeAsync)', fakeAsync(() => {
    fixture.detectChanges();
    tick();                  // wait for async getList
    fixture.detectChanges(); // update view with champion

    expect(comp.championsList).toEqual(jasmine.any(Object));
    el = de.nativeElement;
    expect(el.textContent).toBe(testChampion);
  }));

  it('should show champion after getList promise (done)', (done: any) => {
    fixture.detectChanges();

    // get the list promise and wait for it to resolve
    spy.calls.mostRecent().returnValue.then(() => {
      fixture.detectChanges(); // update view with champion
      el = de.nativeElement;
      expect(el.textContent).toBe(testChampion);
      expect(comp.championsList).toEqual(undefined);
      done();
    });
  });
});
