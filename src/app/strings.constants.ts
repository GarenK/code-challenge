export class Strings {
	// all in app static strings

	// route names
  public static readonly LIST_PAGE_NAME = 'list';
  public static readonly SPI_PAGE_NAME = 'spi';

	// url
  public static readonly API_URL = 'api';
  public static readonly CHAMPION_ANALYTICS_URL = 'champion_analytics.json';
  public static readonly CHAMPION_SPI_URL = 'champion_spi.json';
  public static readonly ASSET_TREND_DOWN = 'assets/iconSPICalculationDown.imageset/iconSPICalculationDown@3x.png';
  public static readonly ASSET_TREND_UP = 'assets/iconSPICalculationUp.imageset/iconSPICalculationUp@3x.png';

  // display text
  public static readonly LIST_PAGE_TITLE = 'DojoMadness Code Challenge';
  public static readonly SPI_PAGE_TITLE = ' SPI';
  public static readonly SPI_PAGE_SUB_TITLE = 'Game Time SPI for ';
  public static readonly EARLY_GAME_TEXT = 'Early Game';
  public static readonly MID_GAME_TEXT = 'Mid Game';
  public static readonly LATE_GAME_TEXT = 'Late Game';
  public static readonly EARLY_GAME_TIME = '0 - 10 m';
  public static readonly MID_GAME_TIME = '10 - 20 m';
  public static readonly LATE_GAME_TIME = '20 - 30 m';
}
