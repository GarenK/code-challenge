import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Strings } from './strings.constants';
import { ChampionListComponent } from './championList.component';
import { ChampionDetailComponent } from './championDetail.component';

const routes: Routes = [
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: Strings.LIST_PAGE_NAME,  component: ChampionListComponent },
  { path: `${Strings.SPI_PAGE_NAME}/:id`,  component: ChampionDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
