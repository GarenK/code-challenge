import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { ChampionService } from './champion.service';
import { ObserverService } from './observer.service';
import { Strings } from './strings.constants';

@Component({
  selector: 'app-champion-detail',
  templateUrl: './championDetail.component.html',
  styleUrls: [ './championDetail.component.sass' ]
})
export class ChampionDetailComponent implements OnInit {
  id: number;
  championData;
  spi;
  phaseText;
  phaseTime;
  subTitle: string;
  transform;
  transformInv;
  error;
  ringColor;

  constructor(
    private route: ActivatedRoute,
    private championService: ChampionService,
    private observerService: ObserverService
  ) {
    this.phaseText = {
      early_game: Strings.EARLY_GAME_TEXT,
      mid_game: Strings.MID_GAME_TEXT,
      late_game: Strings.LATE_GAME_TEXT
    };
    this.phaseTime = {
      early_game: Strings.EARLY_GAME_TIME,
      mid_game: Strings.MID_GAME_TIME,
      late_game: Strings.LATE_GAME_TIME
    };
    this.subTitle = Strings.SPI_PAGE_SUB_TITLE;
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      // get id from the url and retreive all aditional data
      // to make sure the page will still load when the browser is refreshed
      this.championService.getChampionById(this.id)
        .then(data => {
          this.observerService.changeNavigationText(data.champion.name);
          this.championData = data;
        })
        .catch(error => this.error = error);
      this.championService.getSPI(this.id)
        .then(data => {
          this.spi = data.champion_spi;
          setTimeout(this.setTransformValue.bind(this), 200, this.spi.overall);
        })
        .catch(error => this.error = error);
    });
  }

  getBadgeUrl(badge: string) {
    return `assets/${badge}.imageset/${badge}@3x.png`;
  }

  getTrendUrl(trend: string) {
    if (trend === 'down') {
      return Strings.ASSET_TREND_DOWN;
    }
    if (trend === 'up') {
      return Strings.ASSET_TREND_UP;
    }
    return;
  }

  getSPIRate(phase) {
    return (phase.value - phase.lower_limit) / (phase.upper_limit - phase.lower_limit);
  }

  setTransformValue(phase) {
    this.transform = `rotate(${this.getSPIRate(phase) * 180}deg)`;
    this.transformInv = `rotate(${this.getSPIRate(phase) * -180}deg)`;
  }
}
