import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation.component';
import { ChampionListComponent } from './championList.component';
import { ChampionDetailComponent } from './championDetail.component';

import { ChampionService } from './champion.service';
import { ObserverService } from './observer.service';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ChampionListComponent,
    ChampionDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [ChampionService, ObserverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
