import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router, NavigationStart } from '@angular/router';
import { Location } from '@angular/common';
import { ObserverService } from './observer.service';
import { Strings } from './strings.constants';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: [ './navigation.component.sass' ]
})
export class NavigationComponent implements OnInit, OnDestroy {
  listPageName: string;
  spiPageName: string;
  listPageTitle: string;
  spiPageTitle: string;
  titleComponent: string;
  page: string;
  subscription: Subscription;

  constructor(
    private observerService: ObserverService,
    router: Router,
    private _location: Location
  ) {
    this.listPageName = Strings.LIST_PAGE_NAME;
    this.spiPageName = Strings.SPI_PAGE_NAME;
    this.listPageTitle = Strings.LIST_PAGE_TITLE;
    this.spiPageTitle = Strings.SPI_PAGE_TITLE;
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.page = event.url.split('/')[1];
      }
    });
  }
  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.subscription = this.observerService.navigationText$
       .subscribe(text => this.titleComponent = text);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
