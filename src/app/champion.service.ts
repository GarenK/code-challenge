import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Strings } from './strings.constants';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ChampionService {

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {}

  getList(): Promise<any> {
    return this.http.get(`${Strings.API_URL}/${Strings.CHAMPION_ANALYTICS_URL}`)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getChampionById(id: number): Promise<any> {
    return this.http.get(`${Strings.API_URL}/${Strings.CHAMPION_ANALYTICS_URL}`)
      .toPromise()
      .then(response => {
        for (const champion of response.json().champion_analytics) {
          if (champion.champion.id === id) {
            return champion;
          }
        }
      })
      .catch(this.handleError);
  }

  getSPI(id: number): Promise<any> {
    return this.http.get(`${Strings.API_URL}/${Strings.CHAMPION_SPI_URL}`)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  // use .then(this.delayPromise) for testing loading state
  private delayPromise (value) {
    return new Promise(resolve =>
      setTimeout(() => resolve(value), 2000)
    );
  }

  // use .then(this.rejectPromise) for testing loading failed state
  private rejectPromise (value) {
    return new Promise(function(resolve, reject) {
      setTimeout(() => reject(value), 2000);
    });
  }
}
